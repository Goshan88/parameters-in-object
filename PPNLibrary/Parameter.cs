﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PPNLibrary
{
    public class Parameter : Name
    {
        //Событие, возникающее при изменении пареметра
        protected internal event ParameterStateHandler Changed;
        protected internal event ParameterStateHandler Imitation;
        protected internal event ParameterStateHandler ChangeOperation;
        //Входной сигнал
        private float input;
        public float Input { get
            {
                return this.input;
            }
           set {
                this.input = value;
                ParameterTime = DateTime.Now;
                if (!this.IsImitation) this.Output = value;
            } 
            }
        //выходной сигнал
        protected float output;
        public float Output { get {
                return output;
            }
            protected set {
                output = value;
                Change();
            }
        }
        //дата и время получения сигнала
        public DateTime ParameterTime { get; set; }
        //режим имитации вкл/выкл
        private bool isImitation = false;
        public bool IsImitation { 
            get {
                return isImitation;
            } set
            {
                changeImit(value);
                isImitation = value;
            }
        }
        //обозначение операции над сигналом
        internal EOperation LabelOperaton { get; private set; }
        //объект операции с параметром
        internal Operand ObjectOperand;
        //численный параметр имитации
        private float imitValue;
        public float ImitValue {get {
                return this.imitValue;
            }
            set
            {
                this.imitValue = value;
                if (this.IsImitation) this.Output = value;
            }
        }
        //направление параметра(вход/выход)
        public EDirect Direction { get; protected set; }
        //конструктор параметра
        internal  Parameter(string name) : base(name)
        {
            Direction = EDirect.INPUT;
            ObjectOperand = new NoOpeartion();
        }
        internal Parameter(string name, EDirect direct) :base(name)
        {
            Direction = direct;
            ObjectOperand = new NoOpeartion();
        }
        internal Parameter(string name, EOperation operation) : base(name)
        {
            Direction = EDirect.INPUT;
            setOperation(operation);
        }
        internal Parameter(string name, EDirect direct, EOperation operation) : base(name)
        {
            Direction = direct;
            setOperation(operation);
        }

        // вызов событий
        private void CallEvent(ParameterEventArgs e, ParameterStateHandler handler)
        {
            if (e != null)
                handler?.Invoke(this, e);
        }
        // вызов отдельных событий. Для каждого события определяется свой метод
        protected void OnChangedParameter(ParameterEventArgs e)
        {
            CallEvent(e, Changed);
        }
        protected void OnChangedImitation(ParameterEventArgs e)
        {
            CallEvent(e, Imitation);
        }
        protected void OnChangedOperation(ParameterEventArgs e)
        {
            CallEvent(e, ChangeOperation);
        }

        // вызов события изменения величины параметра
        protected void Change()
        {
            OnChangedParameter(new ParameterEventArgs($"Параметер изменился Id параметра: {Id}"));
        }
        // вызов события изменения состояния имитации
        protected void changeImit(bool value)
        {
            if(IsImitation != value)
            {
                string tmp =(value)? " установлена":" снята";
                OnChangedImitation(new ParameterEventArgs($"Имитация параметра с id: " + Id + tmp));
            }
        }
        // вызов изменения операции параметра
        protected void changeOperation(EOperation value)
        {
            OnChangedOperation(new ParameterEventArgs($"Параметр с id: " + Id + " изменена операция на " + value));
        }
        // установка операции параметра
        internal void setOperation(EOperation operation)
        {
            switch (operation)
            {
                case EOperation.NO_OPERATION:
                    ObjectOperand = new NoOpeartion();
                    break;
                case EOperation.ADD:
                    ObjectOperand = new Add();
                    break;
                case EOperation.SUBSTRACTION:
                    ObjectOperand = new Substraction();
                    break;
                case EOperation.MULTIPLICATION:
                    ObjectOperand = new Multiplication();
                    break;
                case EOperation.DIVISION:
                    ObjectOperand = new Division();
                    break;
                default:
                    throw new Exception("No operation: " + operation);
            }
            if (LabelOperaton != operation)
            {
                changeOperation(operation);
                LabelOperaton = operation;
            }
        }
        //текущая операция параметра
        internal EOperation getOperation()
        {
            return LabelOperaton;
        }
        //оператор суммы
        public static float operator + (Parameter p1, Parameter p2)
        {
            float tmp = 0;
            tmp = p1.Output + p2.Output;
            return tmp;
        }
        //оператор вычитания
        public static float operator -(Parameter p1, Parameter p2)
        {
            float tmp = 0;
            tmp = p1.Output - p2.Output;
            return tmp;
        }
        //оператор умножения
        public static float operator *(Parameter p1, Parameter p2)
        {
            float tmp = 0;
            tmp = p1.Output * p2.Output;
            return tmp;
        }
        //оператор деления
        public static float operator /(Parameter p1, Parameter p2)
        {
            float tmp = 0;
            try
            {
                tmp = p1.Output / p2.Output;
            }
            catch (DivideByZeroException e)
            {
                throw new Exception(e.Message + ", parameter name: " + p2.ObjectName);
            }
            return tmp;
        }
        //оператор преобразования в строку
        public override string ToString()
        {
            return String.Format("Id:{0}, Name:{1}, Direction:{2}, Operation:{7}, Input:{3}, Output:{4}, IsImitation:{5}, ImitValue:{6} ",
                                    Id, ObjectName, Direction, Input, Output, IsImitation, ImitValue, LabelOperaton);
        }
    }
}
