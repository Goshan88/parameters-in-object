﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PPNLibrary
{
    public delegate void ParameterStateHandler(object sender, ParameterEventArgs e);
    public class ParameterEventArgs
    {
        //Сообщение
        public string Message { get; private set; }
        public ParameterEventArgs(string mes)
        {
            Message = mes;
        }
    }
}
