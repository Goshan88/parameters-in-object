﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PPNLibrary
{
    public abstract class Operand
    {
        internal virtual float makeOperation(float output, float input)
        {
            return output; //no operation
        }
    }

    internal class NoOpeartion: Operand{
        
    }

    internal class Add : Operand
    {
        internal override float makeOperation(float output, float input)
        {
            return (output + input);
        }
    }

    internal class Substraction : Operand
    {
        internal override float makeOperation(float output, float input)
        {
            return (output - input);
        }
    }

    internal class Multiplication : Operand
    {
        internal override float makeOperation(float output, float input)
        {
            return (output * input);
        }
    }

    internal class Division : Operand
    {
        internal override float makeOperation(float output, float input)
        {
            float tmp = 0;
            try
            {
                tmp = output / input;
            }
            catch (DivideByZeroException e)
            {
                throw new Exception(e.Message);
            }
            return tmp;
        }
    }
}
