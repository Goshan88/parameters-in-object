﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PPNLibrary
{
        public enum EDirect
        {
            INPUT,
            OUTPUT
        }

        public enum EOperation
        {
            NO_OPERATION,
            ADD,
            SUBSTRACTION,
            MULTIPLICATION,
            DIVISION
        }
}
