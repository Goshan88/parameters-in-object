﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace PPNLibrary
{
    public class Shell
    {
        public string Name { get; set; } 
        private List<PPNObject> objects;
        private Task taskCalcParameters;
        private bool isCalculating;
        private int timeUpdate = MIN_TIME_UPDATE;//период обновления данных
        const int MIN_TIME_UPDATE = 1;//минимальное время обновления расчетов
        const int MAX_TIME_UPDATE = 1000;//максимальное время обновления расчетов
        //контсруктор
        public Shell(string name)
        {
            Name = name;
            objects = new List<PPNObject> { };
            startCalculating();
        }

        
        //остановить расчет параметров
        public void stopCalculating()
        {
            isCalculating = false;
        }
        //запустить расчет параметров
        public void startCalculating()
        {
            isCalculating = true;
            if (taskCalcParameters == null)  taskCalcParameters = new Task(() => calcObjectsParameters());
            taskCalcParameters.Start();
        }
        //установить период обновления расчетов
        public void setUpdateTime(int time)
        {
            if (time < MIN_TIME_UPDATE)
            {
                timeUpdate = MIN_TIME_UPDATE;
                return;
            }
            if (time > MAX_TIME_UPDATE)
            {
                timeUpdate = MAX_TIME_UPDATE;
                return;
            }
            timeUpdate = time;
        }

        //добавить объект
        public void addObject(PPNObject subject)
        {
            objects.Add(subject);
        }
        //создать объект
        public PPNObject createObject(String name)
        {
            PPNObject subject = new PPNObject(name);
            addObject(subject);
            return subject;
        }
        //удалить объект
        public void deleteObject(int id)
        {
            var deleteObject = findObject(id);
            objects.Remove(deleteObject);
        }

        //удалить параметр из объекта
        public void deleteParameter(int id)
        {
            //поиск парметра в объектах
            var deleteObject = this.getObjectByParameterId(id);
            deleteObject.deleteParameter(id);
        }

        //ищем объект по его id
        public PPNObject findObject(int id)
        {
            var PPNobject = from o in objects
                                  where (o.Id == id)
                                  select o;

            if (PPNobject.Count() == 0)
            {
                throw new Exception("Объект с id: " + id + " не существует");
            }
            if (PPNobject.Count() > 1)
            {
                throw new Exception("id: " + id + "имеют два объекта");
            }

            return PPNobject.First();
        }
        //ищем объект по его названию
        public PPNObject findObject(string name)
        {
            var PPNobject = from o in objects
                            where (o.ObjectName == name)
                            select o;

            if (PPNobject.Count() == 0)
            {
                throw new Exception("Объект с именем: " + name + " не существует");
            }
            if (PPNobject.Count() > 1)
            {
                throw new Exception("Имя: " + name + "имеют два объекта");
            }

            return PPNobject.First();
        }

        //ищем объект в состав которого входит параметр
        public PPNObject getObjectByParameterId(int id)
        {
            Parameter tmp = null;
            foreach (PPNObject p in objects)
            {
                tmp = p.findParameter(id);
                if (tmp != null) return p;
            }
            throw new Exception("Параметр с id: " + id + " не существует");
        }

        /*расчет параметров объектов*/
        private void calcObjectsParameters()
        {
            while(isCalculating)
            {
                foreach (PPNObject o in objects)
                {
                    if (o.NeedCalc)
                    {
                        try
                        {
                            o.calcParameter();
                        } catch (Exception e)
                        {
                            Console.WriteLine("Исключение в расчетах параметра объекта " + o.ObjectName + " id " + o.Id + " " + e.Message);
                        }
                    }
                }
                Thread.Sleep(timeUpdate);
            }
        }
        /*преобразовать объект в строку*/
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(getShellNameToString());
            var objectOrderName = from p in objects
                                   orderby p.ObjectName //для того чтобы упорядочить названия в алфавитном порядке
                                   select p;
            foreach (PPNObject p in objectOrderName)
            {
                sb.Append(p.ToString() + System.Environment.NewLine);
            }
            return sb.ToString();
        }

        public string getObjectsToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(getShellNameToString());
            var objectsPPN = from p in objects
                                  orderby p.ObjectName //для того чтобы упорядочить названия в алфавитном порядке
                                  select p;
            foreach (PPNObject p in objectsPPN)
            {
                sb.Append(p.getNameToString() + System.Environment.NewLine);
            }
            return sb.ToString();
        }

        public string getParametersToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(getShellNameToString());
            var objectsPPN = from p in objects
                             orderby p.ObjectName //для того чтобы упорядочить названия в алфавитном порядке
                             select p;
            foreach (PPNObject p in objectsPPN)
            {
                sb.Append(p.getParametersToString() + System.Environment.NewLine);
            }
            return sb.ToString();
        }

        public string getParametersNoImitationToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(getShellNameToString());
            var objectsPPN = from p in objects
                             orderby p.ObjectName //для того чтобы упорядочить названия в алфавитном порядке
                             select p;
            foreach (PPNObject p in objectsPPN)
            {
                sb.Append(p.getParametersToString() + System.Environment.NewLine);
            }
            return sb.ToString();
        }

        //вернуть название оболочки
        public string getShellNameToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(String.Format("*****************ППН Name:{0}*****************",
                                    Name));
            sb.Append(System.Environment.NewLine);
            return sb.ToString();
        }
    }
}
