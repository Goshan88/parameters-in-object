﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace PPNLibrary
{
    public class PPNObject: Name, ICalcParameter
    {
        private List<Parameter> parameters;
        public bool NeedCalc { get; private set; }
        //конструктор
        public PPNObject(string name): base(name)
        {
            parameters = new List<Parameter> { };
            //добавляем в объект один выходной сигнал
            string nameParameter = name + "OutputParameter";
            Parameter outParameter = new Parameter(nameParameter, EDirect.OUTPUT);
            addParameter(outParameter);
        }
        //добавить в объект параметр
        public void addParameter(Parameter parameter)
        {
            if(parameter.Direction == EDirect.OUTPUT)
            {
                Parameter getOutputParameter = this.getOutputParameter();
                if (getOutputParameter != null)
                {
                    throw new Exception("Объект с id: " + this.Id.ToString() + " попытка добавить два выходных параметра");
                }
            }
            parameter.Changed += ChangedHandler;
            parameter.Imitation += onChangedImitator;
            parameter.ChangeOperation += onChangedOperation;
            parameters.Add(parameter);
        }
        //создать входной параметр
        public Parameter createParameter(String name, EOperation operation)
        {
            Parameter parameter = new Parameter(name,operation);
            addParameter(parameter);
            return parameter;
        }
        /*удаляем сигнал*/
        public void deleteParameter(int id)
        {
            var outputParameter = this.getOutputParameter();

            if (outputParameter.Id == id)
            {
                throw new Exception("Нельзя удалить выходной сигнал");
            }

            var deleteParameter = findParameter(id);
            deleteParameter.Changed -= ChangedHandler;
            deleteParameter.Imitation -= onChangedImitator;
            deleteParameter.ChangeOperation -= onChangedOperation;
            parameters.Remove(deleteParameter);
            NeedCalc = true;
        }
        //внутри объекта поменялся параметр
        void ChangedHandler(object sender, ParameterEventArgs e)
        {
            NeedCalc = true;
        }
        //внутри объекта включили имитацию параметра
        void onChangedImitator(object sender, ParameterEventArgs e)
        {
            NeedCalc = true;
        }
        //внутри объекта поменялся операнд у параметра
        void onChangedOperation(object sender, ParameterEventArgs e)
        {
            NeedCalc = true;
        }
        /*оператор сложения сигналов*/
        public static PPNObject operator +(PPNObject o, Parameter p)
        {
            p.setOperation(EOperation.ADD);
            o.addParameter(p);
            return o;
        }
        /*оператор вычитания сигналов*/
        public static PPNObject operator -(PPNObject o, Parameter p)
        {
            p.setOperation(EOperation.SUBSTRACTION);
            o.addParameter(p);
            return o;
        }
        /*оператор умножения сигналов*/
        public static PPNObject operator *(PPNObject o, Parameter p)
        {
            p.setOperation(EOperation.MULTIPLICATION);
            o.addParameter(p);
            return o;
        }
        /*оператор деление сигналов*/
        public static PPNObject operator /(PPNObject o, Parameter p)
        {
            p.setOperation(EOperation.DIVISION);
            o.addParameter(p);
            return o;
        }

        /* пока предполается что будет только один параметр
         * указан как выходной в будущем я перепишу эту секцию
         * и сделаю так чтобы выходных параметров могло быть много
         * и каждый входной параметр привязывался к выходному
         */
        public Parameter getOutputParameter()
        {
            var outputParameters = from p in parameters
                                   where (p.Direction == EDirect.OUTPUT)
                                   select p;

            if (outputParameters.Count() == 0)
            {
                return null;//нет выходного параметра
            }
            if (outputParameters.Count() > 1)
            {
                throw new Exception("Объект с id: " + this.Id.ToString() + " не имеет более 1 выходного параметра");
            }
            return outputParameters.First();
        }
        
        //расчет параметров входях сигналов, самый важный метод
        public void calcParameter()
        {
            NeedCalc = false;
            Parameter outputParameter = getOutputParameter();

            /*выбираем все входные параметры*/
            var inputParameters = from p in parameters
                                  where (p.Direction == EDirect.INPUT)
                                  orderby p.LabelOperaton //для того чтобы деление вызывалось последним
                                  select p;
            if (inputParameters == null)
            {
                throw new Exception("Объект с id: " + this.Id.ToString() + " не имеет входных параметров");
            }

            outputParameter.Input = 0;
            /*просчитываем параметры*/
            foreach (Parameter p in inputParameters)
            {
                outputParameter.Input = p.ObjectOperand.makeOperation(outputParameter.Input,p.Output);
            }
        }

        /*ищем сигнал по id*/
        public Parameter findParameter(int id)
        {
            var parameter = from p in parameters
                                  where (p.Id == id)
                                  select p;
            if (parameter.Count() == 0)
            {
                return null;
            }
            if (parameter.Count() > 1)
            {
                throw new Exception("id: " + id + "имеют два сигнала");
            }

            return parameter.First();
        }
        //ищем параметр по названию
        public Parameter findParameter(string  name)
        {
            var parameter = from p in parameters
                            where (p.ObjectName == name)
                            select p;
            if (parameter.Count() == 0)
            {
                return null;
            }
            if (parameter.Count() > 1)
            {
                throw new Exception("Параметр с имененм: " + name + "имеют два сигнала");
            }

            return parameter.First();
        }
        /*преобразовать объект строку*/
        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(String.Format("*****************Объект Id:{0}, Name:{1}*****************",
                                    Id, ObjectName));
            sb.Append(System.Environment.NewLine);
            var objectParameters = from p in parameters
                                   orderby p.Direction //для того чтобы выходной сигнал был последним
                                   select p;
            foreach (Parameter p in objectParameters)
            {
                sb.Append(p.ToString() + System.Environment.NewLine);
            }

            return sb.ToString();
        }
        //вернуть строку с названием объекта
        public string getNameToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(String.Format("*****************Объект Id:{0}, Name:{1}*****************",
                                    Id, ObjectName));
            return sb.ToString();
        }
        /*вернуть строку с перечнем параметров*/
        public string getParametersToString()
        {
            StringBuilder sb = new StringBuilder();
            var objectParameters = from p in parameters
                                   orderby p.Direction //для того чтобы выходной сигнал был последним
                                   select p;
            foreach (Parameter p in objectParameters)
            {
                sb.Append(p.ToString() + System.Environment.NewLine);
            }
            return sb.ToString();
        }

    }
}
