﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PPNLibrary
{
    public abstract class Name
    {
        public long Id { get; private set; }
        public string ObjectName{get; private set;}
        private static long CountId { get; set; }
        public Name(string name)
        {
            ObjectName = name;
            Id = CountId++;
        }
    }
}
