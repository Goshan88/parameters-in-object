﻿using EasyConsole;
using PPNLibrary;
using static PPNLibrary.Shell;

namespace PPN.UserInterface.Pages
{
    class showObjects : MenuPage
    {
        private Shell shell;
        public showObjects(Program program, Shell _shell)
            : base("Показать объекты", program)
        {
            shell = _shell;
        }

        public override void Display()
        {   
            Output.WriteLine(shell.getObjectsToString());

            Input.ReadString("Нажмите [Ввод] для возврата в предыдущие меню");
            Program.NavigateHome();
        }
    }
}
