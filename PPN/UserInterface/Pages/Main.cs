﻿using EasyConsole;

namespace PPN.UserInterface.Pages
{
    class Main : MenuPage
    {
        public Main(Program program)
            : base("Главная страница", program,
                  new Option("Объекты", () => program.NavigateTo<Objects>()),
                  new Option("Параметры", () => program.NavigateTo<Parameters>()))
        {
        }
    }
}
