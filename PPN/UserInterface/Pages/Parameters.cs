﻿using EasyConsole;
using System;
using System.Collections.Generic;
using System.Text;

namespace PPN.UserInterface.Pages
{
    class Parameters : MenuPage
    {
        public Parameters(Program program)
            : base("Параметры", program,
                  new Option("Показать параметры", () => program.NavigateTo<showParameters>()),
                  new Option("Удалить параметр", () => program.NavigateTo<deleteParameter>()),
                  new Option("Создать параметр", () => program.NavigateTo<createParameter>()))
        {
        }
    }
}
