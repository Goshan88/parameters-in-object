﻿using EasyConsole;
using PPNLibrary;
using System;
using static PPNLibrary.Shell;

namespace PPN.UserInterface.Pages
{
    class deleteParameter : MenuPage
    {
        private Shell shell;
        public deleteParameter(Program program, Shell _shell)
            : base("Удалить параметр", program)
        {
            shell = _shell;
        }

        public override void Display()
        {
            Output.WriteLine("Для того, чтобы УДАЛИТЬ параметр, введите id параметра: ", "yellow");
            Output.WriteLine(shell.getParametersToString());
            int id =Input.ReadInt("Введите id параметра от 0 до 2 147 483 647: ", min: 0, max: 2147483647);
            try
            {
                shell.deleteParameter(id);
                Output.WriteLine("Параметр успешно удален");
            }
            catch(Exception e)
            {
                Output.WriteLine("Параметр не удалось удалить " + e.Message , "red");
            }
            Input.ReadString("Нажмите [Ввод] для возврата в главное меню");
            Program.NavigateHome();
        }
    }
}
