﻿using EasyConsole;
using PPNLibrary;
using System;
using static PPNLibrary.Shell;

namespace PPN.UserInterface.Pages
{
    class deleteObject : MenuPage
    {
        private Shell shell;
        public deleteObject(Program program, Shell _shell)
            : base("Удалить объект", program)
        {
            shell = _shell;
        }

        public override void Display()
        {
            Output.WriteLine("Для того, чтобы УДАЛИТЬ объект, введите id объекта: ", "yellow");
            Output.WriteLine(shell.getObjectsToString());
            int id =Input.ReadInt("Введите id объекта от 0 до 2 147 483 647: ", min: 0, max: 2147483647);
            try
            {
                shell.deleteObject(id);
                Output.WriteLine("Объект успешно удален");
            }
            catch(Exception e)
            {
                Output.WriteLine("Объект не удалось удалить " + e.Message , "red");
            }

            Input.ReadString("Нажмите [Ввод] для возврата в главное меню");
            Program.NavigateHome();
        }
    }
}
