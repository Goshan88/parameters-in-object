﻿using EasyConsole;
using PPNLibrary;
using System;
using static PPNLibrary.Shell;

namespace PPN.UserInterface.Pages
{
    class createParameter : MenuPage
    {
        private Shell shell;
        public createParameter(Program program, Shell _shell)
            : base("Создать параметр", program)
        {
            shell = _shell;
        }

        public override void Display()
        {
            Output.WriteLine("Создать параметер: ");
            string name = Input.ReadString("Введите название параметра: ");

            Output.WriteLine("Выберите операнд(операция которая будет выполнятся параметром) для параметра: ");
            Output.WriteLine("0 - нет операции; ");
            Output.WriteLine("1 - сложение; ");
            Output.WriteLine("2 - вычитание; ");
            Output.WriteLine("3 - умножение; ");
            Output.WriteLine("4 - деление. ");

            int operand = Input.ReadInt("Введите операнд от 0 до 4: ", min: 0, max: 4);
            EOperation operandInEOperation = (EOperation)operand;

            Output.WriteLine(shell.getObjectsToString());
            int idObject = Input.ReadInt("Введите id объекта от 0 до 2 147 483 647: ", min: 0, max: 2147483647);
            
            PPNObject objectAddParameter = null;
            try
            {
                objectAddParameter = shell.findObject(idObject);
                Output.WriteLine("Объект успешно найден");
            }
            catch (Exception e)
            {
                Output.WriteLine("Объект не найти " + e.Message, "red");
            }

            try
            {
                objectAddParameter.createParameter(name, operandInEOperation);
                Output.WriteLine("Параметр успешно создан");
            }
            catch(Exception e)
            {
                Output.WriteLine("Параметр не удалось создать " + e.Message , "red");
            }
            Input.ReadString("Нажмите [Ввод] для возврата в главное меню");
            Program.NavigateHome();
        }
    }
}
