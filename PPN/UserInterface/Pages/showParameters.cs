﻿using EasyConsole;
using PPNLibrary;
using static PPNLibrary.Shell;

namespace PPN.UserInterface.Pages
{
    class showParameters : MenuPage
    {
        private Shell shell;
        public showParameters(Program program, Shell _shell)
            : base("Показать объекты", program)
        {
            shell = _shell;
        }

        public override void Display()
        {   
            Output.WriteLine(shell.ToString());

            Input.ReadString("Нажмите [Ввод] для возврата в предыдущие меню");
            Program.NavigateHome();
        }
    }
}
