﻿using EasyConsole;
using PPNLibrary;

namespace PPN.UserInterface.Pages
{
    class Objects : MenuPage
    {
        public Objects(Program program)
            : base("Объекты", program,
                  new Option("Показать объекты", () => program.NavigateTo<showObjects>()),
                  new Option("Добавить объект", () => program.NavigateTo<createObjects>()),
                  new Option("Удалить объект", () => program.NavigateTo<deleteObject>()))
        {
        }
    }
}
