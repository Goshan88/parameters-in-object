﻿using EasyConsole;
using PPNLibrary;
using System;
using static PPNLibrary.Shell;

namespace PPN.UserInterface.Pages
{
    class createObjects : MenuPage
    {
        private Shell shell;
        public createObjects(Program program, Shell _shell)
            : base("Добавить объект", program)
        {
            shell = _shell;
        }

        public override void Display()
        {
            PPNObject newObject = null;

            Output.WriteLine("Добавить объект ");
          
            string name = Input.ReadString("Введите название объекта: ");
            try
            {
                newObject = shell.createObject(name);
            }
            catch(Exception e)
            {
                Output.WriteLine("Объект не создался " + e.Message , "red");
            }

            if(newObject != null)
            {
                String tmp = "Объект успешно создан: " + System.Environment.NewLine;
                tmp += newObject.getNameToString();
                Output.WriteLine(tmp);
            }

            Input.ReadString("Нажмите [Ввод] для возврата в главное меню");
            Program.NavigateHome();
        }
    }
}
