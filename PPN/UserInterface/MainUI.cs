﻿using System;
using System.Collections.Generic;
using System.Text;
using PPN.UserInterface.Pages;
using EasyConsole;
using PPNLibrary;

namespace PPN.UserInterface
{
    class MainUI : Program
    {
        public MainUI(Shell shell) 
            : base("Объекты с параметрами(ППН)", breadcrumbHeader: true)
        {
            AddPage(new Pages.Main(this));
            AddPage(new Pages.Objects(this));
            AddPage(new Pages.Parameters(this));
            AddPage(new Pages.showObjects(this, shell));
            AddPage(new Pages.createObjects(this, shell));
            AddPage(new Pages.deleteObject(this, shell));
            AddPage(new Pages.showParameters(this, shell));
            AddPage(new Pages.deleteParameter(this, shell));
            AddPage(new Pages.createParameter(this, shell));

            SetPage<Main>();
        }
    }
}
