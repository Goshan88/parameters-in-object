﻿using System;
using PPN.UserInterface;
using PPNLibrary;

namespace PPN
{
    class Demo
    {
        static void Main(string[] args)
        {
            Shell ppn = new Shell("Мессояха");
            //задал объекты по умолчанию
            var tmpObject = ppn.createObject("Object1");
            var tmpParameter1 = tmpObject.createParameter("parameter1", EOperation.ADD);
            var tmpParameter2 = tmpObject.createParameter("parameter2", EOperation.ADD);
            tmpParameter1.Input = 10;
            tmpParameter2.Input = 20;
            
            new MainUI(ppn).Run();
        }
    }

}
